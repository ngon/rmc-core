'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _registry = require('./registry');

Object.defineProperty(exports, 'ComponentRegistry', {
  enumerable: true,
  get: function get() {
    return _registry.ComponentRegistry;
  }
});

var _createElement = require('./createElement');

Object.defineProperty(exports, 'createElement', {
  enumerable: true,
  get: function get() {
    return _createElement.createElement;
  }
});
//# sourceMappingURL=index.js.map