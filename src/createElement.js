import { isString as str, isFunction as fn } from './utils'

const $$typeof = Symbol.for('react.element')

function lookup(type, r) {
  return r[type] || type;
}

function map(children, r) {
  return children.map(child => str(child) ? child : render(child, r));
}

function render({ type, props, children }, r = {}) {
  return {
    $$typeof, props, type: lookup(type, r), props, children: map(children, r)
  };
}

export { render as createElement };
