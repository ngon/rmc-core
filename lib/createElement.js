'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createElement = undefined;

var _utils = require('./utils');

var $$typeof = Symbol.for('react.element');

function lookup(type, r) {
  return r[type] || type;
}

function map(children, r) {
  return children.map(function (child) {
    return (0, _utils.isString)(child) ? child : render(child, r);
  });
}

function render(_ref) {
  var type = _ref.type;
  var props = _ref.props;
  var children = _ref.children;
  var r = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  return {
    $$typeof: $$typeof, props: props, type: lookup(type, r), props: props, children: map(children, r)
  };
}

exports.createElement = render;
//# sourceMappingURL=createElement.js.map