'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
  value: true
});
var isString = exports.isString = function isString(o) {
  return typeof o === 'string' || typeof o === 'String';
};
var isFunction = exports.isFunction = function isFunction(o) {
  return typeof o === 'function';
};
var isObject = exports.isObject = function isObject(o) {
  return !isFunction(o) && !isString(o) && (typeof o === 'undefined' ? 'undefined' : _typeof(o)) === 'object';
};
//# sourceMappingURL=utils.js.map